package rps3;

import java.util.ArrayList;

public class GameOutput implements GameListener {

	@Override
	public void onMakeTurn(ArrayList<Player> players) {
		StringBuilder strb = new StringBuilder();
		strb.append("players: ");
		for (Player player : players) {
			strb.append(player + ", ");
		}
		System.out.println(strb.toString());
	}

	@Override
	public void onMakeFigures(ArrayList<Player> players) {
		for (Player player : players) {
			System.out.println(player + " made " + player.getFigure());
		}
	}

	@Override
	public void onGameEnd(Player player) {
		System.out.println(player + " IS THE BIGGEST LOOSER");
	}

}
