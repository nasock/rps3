package rps3;

import java.io.IOException;
import java.util.ArrayList;

public class App {

	private boolean parseIsHuman(String isHumanStr) throws InvalidInputException {
		isHumanStr = isHumanStr.toUpperCase();
		if (isHumanStr.equals("-P")) {
			return true;
		} else if (isHumanStr.equals("-NP")) {
			return false;
		} else {
			throw new InvalidInputException("miss human/not human mark (-P/-NP)");
		}
	}

	public String parseHumanName(String name) throws InvalidInputException {
		if (name == null) {
			name = "YOU";
		}
		return name;
	}

	public int parseBotCount(String stringNum) throws InvalidInputException {
		if (stringNum == null) {
			throw new InvalidInputException("miss bot number");
		}
		stringNum = stringNum.toUpperCase();

		String startStr = stringNum.substring(0, 2);
		if (!startStr.equals("-B")) {
			throw new InvalidInputException("miss bot mark (-B)");
		}

		String countStr = stringNum.substring(stringNum.length() - 1);
		try {
			int count = Integer.parseInt(countStr);
			return count;
		} catch (NumberFormatException e) {
			throw new InvalidInputException(e);
		}
	}

	private ArrayList<Player> makePlayers(boolean isHuman, String name, int count) {
		ArrayList<Player> players = new ArrayList<Player>();
		if (isHuman) {
			players.add(new HumanPlayer(0, name));
		}
		String[] names = new String[] { "a", "b", "c", "d", "e" };
		for (int i = 0; i < count; i++) {
			String aIName = names[i % names.length] + "" + i;
			players.add(new AIPlayer(i, aIName));
		}
		return players;
	}

	private ArrayList<Player> parseStartLine(String[] args) throws InvalidInputException {
		int next = 0;
		boolean isHuman = parseIsHuman(args[next++]);
		String name = null;
		if (isHuman) {
			name = parseHumanName(args[next++]);
		}
		int countBots = parseBotCount(args[next++]);

		ArrayList<Player> players = makePlayers(isHuman, name, countBots);
		return players;
	}

	public static void main(String[] args) {
		App app = new App();
		try {
			ArrayList<Player> players = app.parseStartLine(args);
			int figuresNum = players.size();
			if (figuresNum % 2 == 0) {
				figuresNum = figuresNum + 1;
			}
			RPS3 game = new RPS3(figuresNum);
			GameOutput gOutput = new GameOutput();
			game.addListener(gOutput);
			for (Player player : players) {
				game.addPlayer(player);
			}
			game.play();
		} catch (NoPlayersException | IOException | InvalidInputException e) {
			e.printStackTrace();
		}
	}
}
