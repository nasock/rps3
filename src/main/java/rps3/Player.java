package rps3;

import java.util.ArrayList;

public abstract class Player {
	protected int id;
	protected String name;
	protected Figure figure;

	public Player(int aId, String aName) {
		id = aId;
		name = aName;
		figure = null;
	}

	protected int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Figure getFigure() {
		return figure;
	}

	@Override
	public int hashCode() {
		return id;
	}

	public abstract void init(ArrayList<Figure> figures);

	public abstract Figure makeFigure();

	@Override
	public String toString() {
		return name;
	}

}
