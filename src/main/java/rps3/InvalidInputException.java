package rps3;

public class InvalidInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidInputException(String cause) {
		super(cause);
	}
	
	public InvalidInputException(Throwable cause) {
		super(cause);
	}
}
