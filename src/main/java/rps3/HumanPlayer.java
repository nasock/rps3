package rps3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class HumanPlayer extends Player {
	private BufferedReader reader;
	private HashMap<String, Figure> strsFigures = new HashMap<String, Figure> ();
	private final HashMap<Figure, String> figuresStrs = new HashMap<Figure, String>();

	public HumanPlayer(int aId, String aName) {
		super(aId, aName);
		InputStreamReader isr = new InputStreamReader(System.in);
		reader = new BufferedReader(isr);
		figuresStrs.put(Figure.ROCK, "R");
		figuresStrs.put(Figure.SCISSORS, "C");
		figuresStrs.put(Figure.PAPER, "P");
		figuresStrs.put(Figure.LIZARD, "L");
		figuresStrs.put(Figure.SPOCK, "S");
		figuresStrs.put(Figure.VODKA, "V");
		figuresStrs.put(Figure.DEVIL, "D");
	}

	@Override
	public void init(ArrayList<Figure> figures) {
		for (Figure figure : figures) {
			strsFigures.put(figuresStrs.get(figure), figure);
		}
	}

	private ArrayList<String> parseInput(String s) {
		String[] stringArray = s.split(" ");

		ArrayList<String> strings = new ArrayList<String>();
		for (String st : stringArray) {
			st = st.trim();
			st = st.toUpperCase();
			if (st.length() == 0) {
				continue;
			}
			strings.add(st);
		}
		return strings;
	}

	private String getFigureString() throws IOException, InvalidInputException {
		String line = reader.readLine();
		ArrayList<String> input = parseInput(line);
		if (input.size() == 0 || input.size() > 1) {
			throw new InvalidInputException("wrong size");
		}
		return input.get(0);
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof HumanPlayer)) {
			return false;
		}
		Player otherPlayer = (Player) other;
		return otherPlayer.getId() == id;
	}

	@Override
	public Figure makeFigure() {
		try {
			String figureStr = getFigureString();
			Figure chossenFigure = strsFigures.get(figureStr);
			if (chossenFigure == null) {
				System.out.println("wrong symboll");
				chossenFigure = makeFigure();
			}
			figure = chossenFigure;
			return figure;
		} catch (IOException | InvalidInputException e) {
			System.out.println("enter symboll");
			return makeFigure();
		}
		
	}

}
