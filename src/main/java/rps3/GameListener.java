package rps3;

import java.util.ArrayList;

public interface GameListener {
	public void onMakeTurn(ArrayList<Player> players);
	public void onMakeFigures(ArrayList<Player> players);
	public void onGameEnd(Player player);
}
