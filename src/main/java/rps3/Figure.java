package rps3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public enum Figure {
	ROCK, SCISSORS, PAPER, LIZARD, SPOCK, VODKA, DEVIL;

	private static final List<Figure> VALUES = Arrays.asList(Figure.values());
	private static final HashMap<Figure, ArrayList<Figure>> graphOfLoosing = new HashMap<Figure, ArrayList<Figure>>();

	static {
		graphOfLoosing.put(Figure.ROCK, makeArrayList(Figure.PAPER, Figure.SPOCK, Figure.VODKA));
		graphOfLoosing.put(Figure.SCISSORS, makeArrayList(Figure.ROCK, Figure.SPOCK, Figure.VODKA));
		graphOfLoosing.put(Figure.PAPER, makeArrayList(Figure.SCISSORS, Figure.LIZARD, Figure.DEVIL));
		graphOfLoosing.put(Figure.LIZARD, makeArrayList(Figure.SCISSORS, Figure.ROCK, Figure.DEVIL));
		graphOfLoosing.put(Figure.SPOCK, makeArrayList(Figure.PAPER, Figure.LIZARD, Figure.VODKA));
		graphOfLoosing.put(Figure.VODKA, makeArrayList( Figure.PAPER, Figure.LIZARD, Figure.DEVIL));
		graphOfLoosing.put(Figure.DEVIL, makeArrayList(Figure.ROCK, Figure.SCISSORS, Figure.SPOCK));
	}

	private static ArrayList<Figure> makeArrayList(Figure f1, Figure f2, Figure f3) {
		ArrayList<Figure> winers = new ArrayList<Figure>();
		winers.add(f1);
		winers.add(f2);
		winers.add(f3);
		return winers;
	}

	public static ArrayList<Figure> getFigures(int figuresNum) {
		ArrayList<Figure> figures = new ArrayList<Figure>();
		for (int i = 0; i < figuresNum; i++) {
			figures.add(VALUES.get(i));
		}
		return figures;
	}
	
	public boolean isLoosing(Figure other) {
		ArrayList<Figure> winers = graphOfLoosing.get(this);
		if (winers.contains(other)) {
			return true;
		} else {
			return false;
		}
	}

}
