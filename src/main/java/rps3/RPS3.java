package rps3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class RPS3 {
	private int figuresNum;
	private ArrayList<GameListener> listeners;
	private ArrayList<Player> players;
	private boolean inProgress;
	private ArrayList<Figure> figures;

	public RPS3(int num) {
		figuresNum = num;
		listeners = new ArrayList<GameListener>();
		players = new ArrayList<Player>();
		inProgress = true;
		figures = Figure.getFigures(figuresNum);
	}

	public void addListener(GameListener l) {
		listeners.add(l);
	}

	public void removeListener(GameListener l) {
		listeners.remove(l);
	}

	private void notifyOnMakeTurn(ArrayList<Player> players) {
		for (GameListener l : listeners) {
			l.onMakeTurn(players);
		}
	}

	private void notifyOnMakeFigures(ArrayList<Player> players) {
		for (GameListener l : listeners) {
			l.onMakeFigures(players);
		}
	}

	private void notifyOnGameEnd(Player player) {
		for (GameListener l : listeners) {
			l.onGameEnd(player);
		}
	}

	public void addPlayer(Player player) {
		if (players.size() >= figuresNum) {
			return;
		}
		player.init(figures);
		players.add(player);
	}

	private void putFigureToMap(HashMap<Figure, ArrayList<Player>> figuresToPlayers, Player player, Figure figure) {
		ArrayList<Player> playersForFigure = new ArrayList<Player>();
		if (figuresToPlayers.keySet().contains(figure)) {
			playersForFigure = figuresToPlayers.get(figure);
		}
		playersForFigure.add(player);
		figuresToPlayers.put(figure, playersForFigure);
	}

	private HashMap<Figure, ArrayList<Player>> makeFigures() throws IOException, InvalidInputException {
		HashMap<Figure, ArrayList<Player>> figuresToPlayers = new HashMap<Figure, ArrayList<Player>>();
		for (Player player : players) {
			Figure figure = player.makeFigure();
			putFigureToMap(figuresToPlayers, player, figure);
		}
		notifyOnMakeFigures(players);
		return figuresToPlayers;
	}

	private boolean checkFigureLoosing(Figure figure, Figure[] fArray) {
		for (int i = 0; i < fArray.length; i++) {
			Figure figureForCheck = fArray[i];
			if (figure.isLoosing(figureForCheck)) {
				return true;
			}
		}
		return false;
	}

	private void makeTurn() throws IOException, InvalidInputException {
		notifyOnMakeTurn(players);
		HashMap<Figure, ArrayList<Player>> figuresToPlayers = makeFigures();
		int size = figuresToPlayers.size();
		if (size == 1 || size > (figuresNum - (figuresNum / 2))) {
			return;
		}

		ArrayList<Player> loosers = new ArrayList<Player>();

		Figure[] temp = new Figure[1];
		Figure[] fArray = (Figure[]) figuresToPlayers.keySet().toArray(temp);
		for (int i = 0; i < fArray.length; i++) {
			Figure figure = fArray[i];
			if (checkFigureLoosing(figure, fArray)) {
				loosers.addAll(figuresToPlayers.get(figure));
			}
		}

		players = loosers;
	}

	private boolean checkState() throws NoPlayersException {
		if (players.size() == 0) {
			throw new NoPlayersException();
		} else if (players.size() == 1) {
			inProgress = false;
		}
		return inProgress;
	}

	public void play() throws NoPlayersException, IOException, InvalidInputException {
		while (checkState()) {
			makeTurn();
		}
		notifyOnGameEnd(players.get(0));
	}
}
