package rps3;

import java.util.ArrayList;
import java.util.Random;

public class AIPlayer extends Player {
	private ArrayList<Figure> figures;
	private static final Random RANDOM = new Random();

	public AIPlayer(int id, String aName) {
		super(id, aName);
	}
	
	@Override
	public void init(ArrayList<Figure> aFigures) {
		figures = aFigures;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof AIPlayer)) {
			return false;
		}
		Player otherPlayer = (Player) other;
		return otherPlayer.getId() == id;
	}

	@Override
	public Figure makeFigure() {
		int ind = RANDOM.nextInt(figures.size());
		Figure randomFigure = figures.get(ind);
		figure = randomFigure;
		return randomFigure;
	}
}
